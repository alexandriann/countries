exports.printMsg = function() {
    console.log("This is a message from the demo package");
  }

var countries = require("i18n-iso-countries");
const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  countries.registerLocale(require("i18n-iso-countries/langs/en.json"));
  var impex;
  for (var country in countries.getNames('en')) {
    impex += countries.alpha2ToAlpha3(country + '') + ';' + countries.getName(country + '', 'en')+ ';' + countries.getName(country + '', 'ja') + '\n';
    
  }
  res.end(impex);
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});